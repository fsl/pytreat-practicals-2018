{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Pandas"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pandas is a data analysis library focussed on the cleaning and exploration of tabular data.\n",
    "\n",
    "Some useful links are:\n",
    "- [main website](https://pandas.pydata.org)\n",
    "- [documentation](http://pandas.pydata.org/pandas-docs/stable/)<sup>1</sup>\n",
    "- [Python Data Science Handbook](https://jakevdp.github.io/PythonDataScienceHandbook/)<sup>1</sup> by Jake van der Plas\n",
    "\n",
    "<sup>1</sup> This tutorial borrows heavily from the pandas documentation and the Python Data Science Handbook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline\n",
    "import pandas as pd  # pd is the usual abbreviation for pandas\n",
    "import seaborn as sns  # seaborn is the main plotting library for Pandas\n",
    "import statsmodels.api as sm  # statsmodels fits linear models to pandas data\n",
    "import statsmodels.formula.api as smf\n",
    "from IPython.display import Image\n",
    "sns.set()  # use the prettier seaborn plotting settings rather than the default matplotlib one"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading in data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pandas supports a wide range of I/O tools to load from text files, binary files, and SQL databases. You can find a table with all formats [here](http://pandas.pydata.org/pandas-docs/stable/io.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic = pd.read_csv('https://raw.githubusercontent.com/mwaskom/seaborn-data/master/titanic.csv')\n",
    "titanic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This loads the data into a [DataFrame](https://pandas.pydata.org/pandas-docs/version/0.21/generated/pandas.DataFrame.html) object, which is the main object we will be interacting with in pandas. It represents a table of data.\n",
    "\n",
    "The other file formats all start with `pd.read_{format}`.  Note that we can provide the URL to the dataset, rather than download it beforehand.\n",
    "\n",
    "We can write out the dataset using `dataframe.to_{format}(<filename)`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.to_csv('titanic_copy.csv', index=False)  # we set index to False to prevent pandas from storing the row names"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you can not connect to the internet, you can run the command below to load this locally stored titanic dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "titanic = pd.read_csv('09_pandas/titanic.csv')\n",
    "titanic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the titanic dataset was also available to us as one of the standard datasets included with seaborn. We could load it from there using"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sns.load_dataset('titanic')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dataframes can also be created from other python objects, using pd.DataFrame.from_{other type}. The most useful of these is from_dict, which converts a mapping of the columns to a pandas DataFrame (i.e., table).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "pd.DataFrame.from_dict({\n",
    "    'random numbers': np.random.rand(5),\n",
    "    'sequence (int)': np.arange(5),\n",
    "    'sequence (float)': np.linspace(0, 5, 5),\n",
    "    'letters': list('abcde'),\n",
    "    'constant_value': 'same_value'\n",
    "})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For many applications (e.g., ICA, machine learning input) you might want to extract your data as a numpy array. The underlying numpy array can be accessed using the `values` attribute"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the type of the returned array is the most common type (in this case object). If you just want the numeric parts of the table you can use `select_dtype`, which selects specific columns based on their dtype:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.select_dtypes(include=np.number).values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the numpy array has no information on the column names or row indices.\n",
    "\n",
    "Alternatively, when you want to include the categorical variables in your later analysis (e.g., for machine learning), you can extract dummy variables using: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "pd.get_dummies(titanic)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accessing parts of the data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Documentation on indexing](http://pandas.pydata.org/pandas-docs/stable/indexing.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Selecting columns by name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Single columns can be selected using the normal python indexing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic['embark_town']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the column names are simple strings (not required) we can also access it directly as an attribute"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.embark_town"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that this returns a pandas [Series](https://pandas.pydata.org/pandas-docs/version/0.23.4/generated/pandas.Series.html) rather than a DataFrame object. A Series is simply a 1-dimensional array representing a single column.\n",
    "\n",
    "Multiple columns can be returned by providing a list of columns names. This will return a DataFrame:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic[['class', 'alive']]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that you have to provide a list here (square brackets). If you provide a tuple (round brackets) pandas will think you are trying to access a single column that has that tuple as a name:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic[('class', 'alive')]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case there is no column called ('class', 'alive') leading to an error. Later on we will see some uses to having columns named like this."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Indexing rows by name or integer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Individual rows can be accessed based on their name (i.e., the index) or integer (i.e., which row it is in). In our current table this will give the same results. To ensure that these are different, let's sort our titanic dataset based on the passenger fare:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic_sorted = titanic.sort_values('fare')\n",
    "titanic_sorted"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the re-sorting did not change the values in the index (i.e., left-most column).\n",
    "\n",
    "We can select the first row of this newly sorted table using iloc"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic_sorted.iloc[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can select the row with the index 0 using"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic_sorted.loc[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that this gives the same passenger as the first row of the initial table before sorting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.iloc[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another common way to access the first or last N rows of a table is using the head/tail methods"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic_sorted.head(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic_sorted.tail(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that nearly all methods in pandas return a new Dataframe, which means that we can easily call another method on them"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic_sorted.tail(10).head(5)  # select the first 5 of the last 10 passengers in the database"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic_sorted.iloc[-10:-5]  # alternative way to get the same passengers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Exercise: use sorting and tail/head or indexing to find the 10 youngest passengers on the titanic. Try to do this on a single line by chaining calls to the titanic dataframe object"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.sort_values..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Indexing rows by value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One final way to select specific columns is by their value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic[titanic.sex == 'female']  # selects all females"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# select all passengers older than 60 who departed from Southampton\n",
    "titanic[(titanic.age > 60) & (titanic['embark_town'] == 'Southampton')]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that this required typing \"titanic\" quite often. A quicker way to get the same result is using the `query` method, which is described in detail [here](http://pandas.pydata.org/pandas-docs/stable/indexing.html#the-query-method) (note that using the `query` method is also faster and uses a lot less memory).\n",
    "\n",
    "> You may have trouble using the query method with columns which have a name that cannot be used as a Python identifier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.query('(age > 60) & (embark_town == \"Southampton\")')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Particularly useful when selecting data like this is the `isna` method which finds all missing data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic[~titanic.age.isna()]  # select first few passengers whose age is not N/A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This removing of missing numbers is so common that it has is own method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.dropna()  # drops all passengers that have some datapoint missing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.dropna(subset=['age', 'fare'])  # Only drop passengers with missing ages or fares"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Exercise: use sorting, indexing by value, dropna and tail/head or indexing to find the 10 oldest female passengers on the titanic. Try to do this on a single line by chaining calls to the titanic dataframe object"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting the data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we start analyzing the data, let's play around with visualizing it. \n",
    "\n",
    "Pandas does have some basic built-in plotting options:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.fare.hist(bins=20, log=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.age.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Individual columns are essentially 1D arrays, so we can use them as such in matplotlib"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "plt.scatter(titanic.age, titanic.fare)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, for most purposes much nicer plots can be obtained using [Seaborn](https://seaborn.pydata.org). Seaborn has support to produce plots showing the [univariate](https://seaborn.pydata.org/tutorial/distributions.html#plotting-univariate-distributions) or [bivariate](https://seaborn.pydata.org/tutorial/distributions.html#plotting-bivariate-distributions) distribution of data in a single or a grid of plots.\n",
    "\n",
    "Most of the seaborn plotting functions expect to get a pandas dataframe (although they will work with Numpy arrays as well). So we can plot age vs. fare like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sns.jointplot('age', 'fare', data=titanic)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Exercise: check the documentation from `sns.jointplot` (hover the mouse over the text \"jointplot\" and press shift-tab) to find out how to turn the scatter plot into a density (kde) map"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sns.jointplot('age', 'fare', data=titanic, ...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is just a brief example of how we can use multiple columns to illustrate the data in more detail"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sns.relplot(x='age', y='fare', col='class', hue='sex', data=titanic,\n",
    "           col_order=('First', 'Second', 'Third'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Exercise: Split the plot above into two rows with the first row including the passengers who survived and the second row those who did not (you might have to check the documentation again by using shift-tab while overing the mouse over `relplot`) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sns.relplot(x='age', y='fare', col='class', hue='sex', data=titanic,\n",
    "           col_order=('First', 'Second', 'Third')...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the nice thing of Seaborn is how easy it is to update how these plots look. You can read more about that [here](https://seaborn.pydata.org/tutorial/aesthetics.html). For example, to increase the font size to get a plot more approriate for a talk, you can use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sns.set_context('talk')\n",
    "sns.violinplot(x='class', y='age', hue='sex', data=titanic, split=True, \n",
    "               order=('First', 'Second', 'Third'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summarizing the data (mean, std, etc.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a large number of built-in methods to summarize the observations in a Pandas dataframe. Most of these will return a Series with the columns names as index:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.quantile(0.75)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One very useful one is `describe`, which gives an overview of many common summary measures"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that non-numeric columns are ignored when summarizing data in this way.\n",
    "\n",
    "We can also define our own functions to apply to the columns (in this case we have to explicitly set the data types)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def mad(series):\n",
    "    \"\"\"\n",
    "    Computes the median absolute deviatation (MAD)\n",
    "    \n",
    "    This is a outlier-resistant measure of the standard deviation\n",
    "    \"\"\"\n",
    "    no_nan = series.dropna()\n",
    "    return np.median(abs(no_nan - np.nanmedian(no_nan)))\n",
    "\n",
    "titanic.select_dtypes(np.number).apply(mad)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also provide multiple functions to the `apply` method (note that functions can be provided as strings)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.select_dtypes(np.number).apply(['mean', np.median, np.std, mad])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Grouping by"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the more powerful features of is `groupby`, which splits the dataset on a categorical variable. The book contains a clear tutorial on that feature [here](https://jakevdp.github.io/PythonDataScienceHandbook/03.08-aggregation-and-grouping.html). You can check the pandas documentation [here](http://pandas.pydata.org/pandas-docs/stable/groupby.html) for a more formal introduction. One simple use is just to put it into a loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for cls, part_table in titanic.groupby('class'):\n",
    "    print(f'Mean fare in {cls.lower()} class: {part_table.fare.mean()}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, it is more often combined with one of the aggregation functions discussed above as illustrated in this figure from the [Python data science handbook](https://jakevdp.github.io/PythonDataScienceHandbook/06.00-figure-code.html#Split-Apply-Combine)\n",
    "\n",
    "![group by image](09_pandas/group_by.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.groupby('class').mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also group by multiple variables at once"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.groupby(['class', 'survived']).mean()  # as always in pandas supply multiple column names as lists, not tuples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When grouping it can help to use the `cut` method to split a continuous variable into a categorical one"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.groupby(['class', pd.cut(titanic.age, bins=(0, 18, 50, np.inf))]).mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the `aggregate` method to apply a different function to each series"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.groupby(['class', 'survived']).aggregate((np.median, mad))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that both the index (on the left) and the column names (on the top) now have multiple levels. Such a multi-level index is referred to as `MultiIndex`. This does complicate selecting specific columns/rows. You can read more of using `MultiIndex` [here](http://pandas.pydata.org/pandas-docs/stable/advanced.html).\n",
    "\n",
    "The short version is that columns can be selected using direct indexing (as discussed above)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_full = titanic.groupby(['class', 'survived']).aggregate((np.median, mad))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_full[('age', 'median')]  # selects median age column; note that the round brackets are optional"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_full['age']  # selects both age columns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember that indexing based on the index was done through `loc`. The rest is the same as for the columns above"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_full.loc[('First', 0)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_full.loc['First']\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More advanced use of the `MultiIndex` is possible through `xs`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_full.xs(0, level='survived') # selects all the zero's from the survived index"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_full.xs('mad', axis=1, level=1) # selects mad from the second level in the columns (i.e., axis=1) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reshaping tables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we were interested in how the survival rate depends on the class and sex of the passengers we could simply use a groupby:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.groupby(['class', 'sex']).survived.mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, this single-column table is difficult to read. The reason for this is that the indexing is multi-leveled (called `MultiIndex` in pandas), while there is only a single column. We would like to move one of the levels in the index to the columns. This can be done using `stack`/`unstack`:\n",
    "- `unstack`: Moves one levels in the index to the columns\n",
    "- `stack`: Moves one of levels in the columns to the index"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.groupby(['class', 'sex']).survived.mean().unstack('sex')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The former table, where the different groups are defined in different rows, is often referred to as long-form. After unstacking the table is often referred to as wide-form as the different group (sex in this case) is now represented as different columns. In pandas some operations are easier on long-form tables (e.g., `groupby`) while others require wide_form tables (e.g., making scatter plots of two variables). You can go back and forth using `unstack` or `stack` as illustrated above, but as this is a crucial part of pandas there are many alternatives, such as `pivot_table`, `melt`, and `wide_to_long`, which we will discuss below.\n",
    "\n",
    "We can prettify the table further using seaborn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "ax = sns.heatmap(titanic.groupby(['class', 'sex']).survived.mean().unstack('sex'), \n",
    "                 annot=True)\n",
    "ax.set_title('survival rate')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that there are also many ways to produce prettier tables in pandas (e.g., color all the negative values). This is documented [here](http://pandas.pydata.org/pandas-docs/stable/style.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because this stacking/unstacking is fairly common after a groupby operation, there is a shortcut for it: `pivot_table`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.pivot_table('survived', 'class', 'sex')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As usual in pandas, where we can also provide multiple column names"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sns.heatmap(titanic.pivot_table('survived', ['class', 'embark_town'], ['sex', pd.cut(titanic.age, (0, 18, np.inf))]), annot=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also change the function to be used to aggregate the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sns.heatmap(titanic.pivot_table('survived', ['class', 'embark_town'], ['sex', pd.cut(titanic.age, (0, 18, np.inf))], \n",
    "                                aggfunc='count'), annot=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As in `groupby` the aggregation function can be a string of a common aggregation function, or any function that should be applied.\n",
    "\n",
    "We can even apply different aggregate functions to different columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "titanic.pivot_table(index='class', columns='sex',  \n",
    "                    aggfunc={'survived': 'count', 'fare': np.mean}) # compute number of survivors and mean fare\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The opposite of `pivot_table` is `melt`. This can be used to change a wide-form table into a long-form table. This is not particularly useful on the titanic dataset, so let's create a new table where this might be useful. Let's say we have a dataset listing the FA and MD values in various WM tracts:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "tracts = ('Corpus callosum', 'Internal capsule', 'SLF', 'Arcuate fasciculus')\n",
    "df_wide = pd.DataFrame.from_dict(dict({'subject': list('ABCDEFGHIJ')}, **{\n",
    "    f'FA({tract})': np.random.rand(10) for tract in tracts }, **{\n",
    "    f'MD({tract})': np.random.rand(10) * 1e-3 for tract in tracts\n",
    "}))\n",
    "df_wide"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This wide-form table (i.e., all the information is in different columns) makes it hard to select just all the FA values or only the values associated with the SLF. For this it would be easier to lismt all the values in a single column. Most of the tools discussed above (e.g., `group_by` or `seaborn` plotting) work better with long-form data, which we can obtain from `melt`: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_long = df_wide.melt('subject', var_name='measurement', value_name='dti_value')\n",
    "df_long.head(12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that `melt` took all the columns (we could also have specified a specific sub-set) and returned each measurement as a seperate row. We probably want to seperate the measurement column into the measurement type (FA or MD) and the tract name. Many string manipulation function are available in the `DataFrame` object under `DataFrame.str` ([tutorial](http://pandas.pydata.org/pandas-docs/stable/text.html))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_long['variable'] = df_long.measurement.str.slice(0, 2)  # first two letters correspond to FA or MD\n",
    "df_long['tract'] = df_long.measurement.str.slice(3, -1)  # fourth till the second-to-last letter correspond to the tract\n",
    "df_long.head(12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we probably do want the FA and MD variables as different columns. \n",
    "\n",
    "*Exercise*: Use `pivot_table` or `stack`/`unstack` to create a column for MD and FA."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_unstacked = df_long."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now use the tools discussed above to visualize the table (`seaborn`) or to group the table based on tract (`groupby` or `pivot_table`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# feel free to analyze this random data in more detail"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general pandas is better at handling long-form than wide-form data, although for better visualization of the data an intermediate format is often best. One exception is calculating a covariance (`DataFrame.cov`) or correlation (`DataFrame.corr`) matrices which computes the correlation between each column:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sns.heatmap(df_wide.corr(), cmap=sns.diverging_palette(240, 10, s=99, n=300), )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear fitting (statsmodels)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Linear fitting between the different columns is available through the [statsmodels](https://www.statsmodels.org/stable/index.html) library. A nice way to play around with a wide variety of possible models is to use R-style functions. The usage of the functions in stastmodels is described [here](https://www.statsmodels.org/dev/example_formulas.html). You can find a more detailed description of the R-style functions [here](https://patsy.readthedocs.io/en/latest/formulas.html#the-formula-language). \n",
    "\n",
    "In short these functions describe the linear model as a string. For example, \"y ~ x + a + x * a\" fits the variable `y` as a function of `x`, `a`, and the interaction between `x` and `a`. The intercept is included by default (you can add \"+ 0\" to remove it)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "result = smf.logit('survived ~ age + sex + age * sex', data=titanic).fit()\n",
    "print(result.summary())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that statsmodels understands categorical variables and automatically replaces them with dummy variables.\n",
    "\n",
    "Above we used logistic regression, which is appropriate for the binary survival rate. A wide variety of linear models are available. Let's try a GLM, but assume that the fare is drawn from a Gamma distribution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "age_dmean = titanic.age - titanic.age.mean()\n",
    "result = smf.glm('fare ~ age_dmean + embark_town', data=titanic).fit()\n",
    "print(result.summary())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Cherbourg passengers clearly paid a lot more...\n",
    "\n",
    "\n",
    "Note that we did not actually add the age_dmean to the dataframe. Statsmodels (or more precisely the underlying [patsy](https://patsy.readthedocs.io/en/latest/) library) automatically extracted this from our environment. This can lead to confusing behaviour..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# More reading"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other useful features\n",
    "- [Concatenating](https://jakevdp.github.io/PythonDataScienceHandbook/03.06-concat-and-append.html) and [merging](https://jakevdp.github.io/PythonDataScienceHandbook/03.07-merge-and-join.html) of tables\n",
    "- [Lots of](http://pandas.pydata.org/pandas-docs/stable/basics.html#dt-accessor) [time](http://pandas.pydata.org/pandas-docs/stable/timeseries.html) [series](http://pandas.pydata.org/pandas-docs/stable/timedeltas.html) support\n",
    "- [Rolling Window functions](http://pandas.pydata.org/pandas-docs/stable/computation.html#window-functions) for after you have meaningfully sorted your data\n",
    "- and much, much more"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.2"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "225px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
